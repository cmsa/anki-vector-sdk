# To Build
# SDK_VERSION=0.6.0; docker build --pull --build-arg SDK_VERSION=$SDK_VERSION -t anki-vector-sdk:latest .
#
# To Run
# mkdir -p ~/Documents/anki-sdk-project; docker run -it --net host -v ~/Documents/anki-sdk-project:/home/vector/vector anki-vector-sdk:latest bash


ARG FROM_IMG=ubuntu:18.04
FROM $FROM_IMG

LABEL maintainer="Carlos Almeida <cmsalmeida@gmail.com>"
LABEL support="https://gitlab.com/cmsa/anki-vector-sdk/issues"

SHELL ["/bin/bash", "-c"]

ARG SDK_VERSION=0.6.0
ENV SDK_VERSION=$SDK_VERSION

# Configure TimeZone
ARG AREA=Europe
ARG ZONE=Lisbon
ENV AREA=$AREA
ENV ZONE=$ZONE

RUN echo "tzdata tzdata/Areas select $AREA" | debconf-set-selections \
    && echo "tzdata tzdata/Zones/Europe select $ZONE" | debconf-set-selections \
    && apt-get update && apt-get install -y --no-install-recommends \
        tzdata

# Install base packages
RUN apt-get update && apt-get install -y --no-install-recommends \
        git \
        python3 \
        python3-dev \
        python3-opencv \
        python3-opengl \
        python3-pil.imagetk \
        python3-pip \
        python3-setuptools \
        vim.tiny \
    && python3 -m pip install --no-cache-dir --upgrade pip \
    && apt-get -y autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && sync

# Install Anki Vector SDK
RUN pip3 install \
    anki_vector==$SDK_VERSION \
    && sync

RUN useradd \
        --shell /bin/bash \
        -m \
        vector
        
USER vector
WORKDIR /home/vector

#CMD /bin/bash -c 'echo DONE; sleep infinity'

CMD /bin/bash
