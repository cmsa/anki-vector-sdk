# Anki Vector SDK

Welcome to the Vector SDK Alpha Docker Container

The Vector SDK gives you direct access to Vector’s unprecedented set of advanced sensors, AI capabilities, and robotics technologies including computer vision, intelligent mapping and navigation, and a groundbreaking collection of expressive animations.

It’s powerful but easy to use, complex but not complicated, and versatile enough to be used across a wide range of domains including enterprise, research, and entertainment.

Please note this is an alpha version of the Vector SDK that is not yet feature complete, but already provides access to many of Vector’s hardware and software features. Please visit the official Anki Developer Forums for more details.

This container provide the latest SDK to enable any one to access to many of Vector’s hardware and software features in a Docker format. [Docker](http://docker.com) is an open platform for developing, shipping, and running applications and services. Docker enables you to separate your services from your infrastructure so you can ship quickly. With Docker, you can manage your infrastructure in the same ways you manage your services. By taking advantage of Docker's methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

https://sdk-resources.anki.com/vector/0.6.0/anki_vector_sdk_examples_0.6.0.tar.gz


# Anki Developer Forums
Please visit the [Anki Developer Forums](https://forums.anki.com/) to ask questions, find solutions and for general discussion.

Anki provides example programs for novice and advanced users to run with the SDK. Download the SDK example programs here:

- SDK Examples
- [macOS/Linux SDK Examples (tar.gz)](https://sdk-resources.anki.com/vector/0.6.0/anki_vector_sdk_examples_0.6.0.tar.gz)
- [Windows SDK Examples (zip)](https://sdk-resources.anki.com/vector/0.6.0/anki_vector_sdk_examples_0.6.0.zip)

Also check the [Get Started](https://sdk-resources.anki.com/vector/docs/getstarted.html).
https://developer.anki.com/vector/docs/getstarted.html

# Requirements
Install [Docker](http://docker.com).

For more information, check: https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce

# Start
docker run \
  -d \
  -v ~/vector.toml:/etc/vector/vector.toml:ro \
  -p 8383:8383 \
  timberio/vector:latest-alpine
 explain this command

# Stop
```
docker stop timberio/vector
```

# Reload
```
docker kill --signal=HUP timberio/vector
```

# Restart
```
docker restart -f $(docker ps -aqf "name=vector")
```

# Observe
```
docker logs -f $(docker ps -aqf "name=vector")
```

# Uninstall
```
docker rm timberio/vector timberio/vector
```

